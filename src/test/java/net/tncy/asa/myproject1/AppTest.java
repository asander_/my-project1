package net.tncy.asa.myproject1;

import org.junit.Test;

import static net.tncy.asa.myproject1.Calculator.add;
import static net.tncy.asa.myproject1.Calculator.neg;
import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void additionCommutativity() {
        int a = 3;
        int b = 12;
        assertEquals(add(a, b), add(b, a));
    }

    @Test
    public void negWorksFine() {
        int a = 5;
        assertEquals(neg(neg(5)), a);
    }

    @Test
    public void oopsieDoopsnt() {
        assertTrue(true);
    }
}
